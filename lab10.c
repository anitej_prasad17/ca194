#include <stdio.h>
int main()
{
   int number1, number2, add, subtract, multiply;
   float divide;
   printf("Enter two integers\n");
   scanf("%d%d", &number1, &number2);
   add = number1 + number2;
   subtract = number1 - number2;
   multiply = number1*number2;
   divide = number1/(float)number2;
   printf("Sum = %d\n", add);
   printf("Difference = %d\n", subtract);
   printf("Multiplication = %d\n", multiply);
   printf("Division = %.2f\n", divide);
   return 0;
}